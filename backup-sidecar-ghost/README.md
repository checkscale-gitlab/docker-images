# `backup-sidecar-ghost`

A sidecar container for running continuous backups on a [`ghost`][ghost] instance. The sidecar takes backups of the local disk and

## Configuration

| ENV                                    | Default                          | Example                          | Description                                                                                                |
|----------------------------------------|----------------------------------|----------------------------------|------------------------------------------------------------------------------------------------------------|
| `OPERATION`                            | `backup`                         | `restore`                        | Operation to perform with the sidecar container                                                            |
| `BACKUP_NAME_PREFIX`                   | `backup`                         | `my-special-backup`              | Prefix that will be used before date of backup                                                             |
| `BACKUP_NAME`                          | `backup-$(date +%F@%H_%M_%S-%Z)` | `backup-2021-10-13@13_47_28-UTC` | Backup name (`.tar.gz` will be appended, `$BUCKET_PREFIX` will be prepended), usually used during restores |
| `GHOST_DATA_DIR`                       | `/data`                          | `/data`                          | Directory containing ghost data                                                                            |
| `GHOST_SQLITE_MAIN_DB_FILENAME`        | `imapsql.db`                     | `ghost.sqlite`                   | Filename of the main ghost SQLite DB                                                                       |
| `GHOST_SQLITE_CREDENTIALS_DB_FILENAME` | `credentials.db`                 | `ghost.credentials.sqlite`       | Filename of the SQLite DB containing credentials                                                           |
| `BACKUP_SCRATCH_DIR`                   | `/tmp`                           | `/backup`                        | Directory that will temporarily house backup                                                               |
| `BACKUP_INTERVAL_SECONDS`              | `43200` (12h)                    | `86400` (24h)                    | Backup interval in seconds                                                                                 |
| `BUCKET`                               | `backups`                        | `backups`                        | Top level bucket name                                                                                      |
| `BUCKET_PREFIX`                        | `ghost/backups`                  | `your/dir/structure`             | Directory struture after bucket name, but before date (ex. `$BUCKET/$BUCKET_PREFIX/$BACKUP_NAME`)          |
| `B2_ACCOUNT_ID`                        | N/A                              | N/A                              | [Backblaze B2][backblaze-b2] Account ID                                                                    |
| `B2_KEY`                               | N/A                              | N/A                              | [Backblaze B2][backblaze-b2] Key                                                                           |

[ghost]: https://ghost.org
[backblaze-b2]: https://www.backblaze.com/b2
