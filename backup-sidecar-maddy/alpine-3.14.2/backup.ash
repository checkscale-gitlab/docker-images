#!/usr/bin/env ash
set -e

# Check provided ENV
if [ -z "$BACKUP_SCRATCH_DIR" ]; then
    BACKUP_SCRATCH_DIR=/tmp
fi

if [ -z "$MADDY_DATA_DIR" ]; then
    MADDY_DATA_DIR=/data
fi

if [ -z "$MADDY_SQLITE_MAIN_DB_FILENAME" ]; then
    MADDY_SQLITE_MAIN_DB_FILENAME=imapsql.db
fi

if [ -z "$MADDY_SQLITE_CREDENTIALS_DB_FILENAME" ]; then
    MADDY_SQLITE_CREDENTIALS_DB_FILENAME=credentials.db
fi

if [ -z "$INTERVAL_SECONDS" ]; then
    INTERVAL_SECONDS=43200
fi

if [ -z "$BUCKET" ]; then
    BUCKET=backups
fi

if [ -z "$BUCKET_PREFIX" ]; then
    BUCKET_PREFIX=maddy
fi

if [ -z "$B2_ACCOUNT_ID" ]; then
    echo -e "B2_ACCOUNT_ID not set"
    exit -1
fi

if [ -z "$B2_KEY" ]; then
    echo -e "B2_KEY not set"
    exit -1
fi

if [ -z "$BACKUP_NAME_PREFIX" ]; then
    BACKUP_NAME_PREFIX=backup
fi

BACKUP_NAME=${BACKUP_NAME_PREFIX}-`date +%F@%H_%M_%S-%Z`
echo -e "[info] BACKUP_NAME=${BACKUP_NAME}"

export COMPRESSED_BACKUP_NAME=${BACKUP_NAME}.tar.gz
echo -e "[info] COMPRESSED_BACKUP_NAME=${COMPRESSED_BACKUP_NAME}"

export BACKUP_DIR=${BACKUP_SCRATCH_DIR}/${BACKUP_NAME}
echo -e "[info] BACKUP_DIR=${BACKUP_DIR}"

export BACKUP_FILE_PATH=${BACKUP_SCRATCH_DIR}/${COMPRESSED_BACKUP_NAME}
echo -e "[info] BACKUP_FILE_PATH=${BACKUP_FILE_PATH}"

# Create backup dir
mkdir -p ${BACKUP_DIR}

# Backup main SQLite DB
export MADDY_SQLITE_MAIN_DB_PATH=${MADDY_DATA_DIR}/${MADDY_SQLITE_MAIN_DB_FILENAME}
export MADDY_SQLITE_MAIN_DB_BACKUP_PATH=${BACKUP_DIR}/${MADDY_SQLITE_MAIN_DB_FILENAME}
echo "[info] taking SQLite backup from file @ [${MADDY_SQLITE_MAIN_DB_PATH}]..."
sqlite3 ${MADDY_SQLITE_MAIN_DB_PATH} ".backup '${MADDY_SQLITE_MAIN_DB_BACKUP_PATH}'"
echo -e "[info] main SQLite DB backup taken, @ [${MADDY_SQLITE_MAIN_DB_BACKUP_PATH}]"

# Backup credentials SQLite DB
export MADDY_SQLITE_CREDENTIALS_DB_PATH=${MADDY_DATA_DIR}/${MADDY_SQLITE_CREDENTIALS_DB_FILENAME}
export MADDY_SQLITE_CREDENTIALS_DB_BACKUP_PATH=${BACKUP_DIR}/${MADDY_SQLITE_CREDENTIALS_DB_FILENAME}
echo "[info] taking SQLite backup from file @ [${MADDY_SQLITE_CREDENTIALS_DB_PATH}]..."
sqlite3 ${MADDY_SQLITE_CREDENTIALS_DB_PATH} ".backup '${MADDY_SQLITE_CREDENTIALS_DB_BACKUP_PATH}'"
echo -e "[info] credential SQLite DB backup taken, @ [${MADDY_SQLITE_CREDENTIALS_DB_BACKUP_PATH}]"

# Backup ancillary data (messages)
export MESSAGES_DIR=${MADDY_DATA_DIR}/messages
export MESSAGES_BACKUP_DIR=${BACKUP_DIR}/messages
cp -r ${MESSAGES_DIR} ${MESSAGES_BACKUP_DIR}

# Backup ancillary data (DKIM keys)
export DKIM_KEYS_DIR=${MADDY_DATA_DIR}/dkim_keys
export DKIM_KEYS_BACKUP_DIR=${BACKUP_DIR}/dkim_keys
cp -r ${DKIM_KEYS_DIR} ${DKIM_KEYS_BACKUP_DIR}

# Backup ancillary data (maddy configuration)
export MADDY_CONF_PATH=${MADDY_DATA_DIR}/maddy.conf
export MADDY_CONF_BACKUP_PATH=${BACKUP_DIR}/maddy.conf
cp -r ${MADDY_CONF_PATH} ${MADDY_CONF_BACKUP_PATH}

# Backup ancillary data (remote queue)
export MADDY_REMOTE_QUEUE_PATH=${MADDY_DATA_DIR}/remote_queue
export MADDY_REMOTE_QUEUE_BACKUP_PATH=${BACKUP_DIR}/remote_queue
cp -r ${MADDY_REMOTE_QUEUE_PATH} ${MADDY_REMOTE_QUEUE_BACKUP_PATH}

# Print backup size
export BACKUP_SIZE=$(du -hs ${BACKUP_DIR})
echo -e "[info] SQLite Backup size: [${BACKUP_SIZE}]"

echo -e "[info] Zipping SQLite backup..."
cd ${BACKUP_SCRATCH_DIR} && tar -czf ${COMPRESSED_BACKUP_NAME} ${BACKUP_NAME}

export REMOTE_BACKUP_DIR=:b2:$BUCKET/${BUCKET_PREFIX}
echo -e "[info] saving SQLite backup to Backblaze under account [${B2_ACCOUNT_ID}]..."
echo -e "[info] saving backup to [${REMOTE_BACKUP_DIR}]"
rclone copy \
  --b2-account $B2_ACCOUNT_ID \
  --b2-key $B2_KEY \
  ${BACKUP_FILE_PATH} \
  ${REMOTE_BACKUP_DIR}

echo "[info] Backup completed, saved to [${REMOTE_BACKUP_DIR}/${COMPRESSED_BACKUP_NAME}]"
