#!/usr/bin/env ash
set -e

# Check provided ENV
if [ -z "$OPERATION" ]; then
    OPERATION=backup
fi

if [ -z "$BACKUP_INTERVAL_SECONDS" ]; then
    BACKUP_INTERVAL_SECONDS=43200
fi

if [ "${OPERATION}" = "backup" ]; then
    while true; do
        echo -e "[info] taking backup..."
        . /backup.ash
        echo -e "[info] sleeping for [${BACKUP_INTERVAL_SECONDS}s]..."
        sleep ${BACKUP_INTERVAL_SECONDS}
    done
elif [ "${OPERATION}" = "restore" ]; then
    echo -e "[info] restoring backup..."
    . /restore.ash
else
    echo -e "[error] unknown operation [${OPERATION}]"
fi
