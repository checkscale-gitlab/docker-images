#!/usr/bin/env ash
set -e

# Check provided ENV
if [ -z "$BACKUP_SCRATCH_DIR" ]; then
    BACKUP_SCRATCH_DIR=/tmp
fi

if [ -z "$INTERVAL_SECONDS" ]; then
    INTERVAL_SECONDS=43200
fi

if [ -z "$BUCKET" ]; then
    BUCKET=backups
fi

if [ -z "$BUCKET_PREFIX" ]; then
    BUCKET_PREFIX=postgres
fi

if [ -z "$PGHOST" ]; then
    echo -e "PGHOST not set"
    exit -1
fi

if [ -z "$PGUSER" ]; then
    echo -e "PGUSER not set"
    exit -1
fi

if [ -z "$PGPASSWORD" ]; then
    echo -e "PGPASSWORD not set"
    exit -1
fi

if [ -z "$B2_ACCOUNT_ID" ]; then
    echo -e "B2_ACCOUNT_ID not set"
    exit -1
fi

if [ -z "$B2_KEY" ]; then
    echo -e "B2_KEY not set"
    exit -1
fi

if [ -z "$BACKUP_NAME" ]; then
    echo -e "BACKUP_NAME must be set for a restore"
    exit -1
fi
echo -e "[info] BACKUP_NAME=${BACKUP_NAME}"

echo -e "[info] clearing the scratch directory..."
mkdir -p ${BACKUP_SCRATCH_DIR}
rm -rf ${BACKUP_SCRATCH_DIR}/*

export COMPRESSED_BACKUP_NAME=${BACKUP_NAME}.tar.gz
echo -e "[info] COMPRESSED_BACKUP_NAME=${COMPRESSED_BACKUP_NAME}"

export BACKUP_FILE_PATH=${BACKUP_SCRATCH_DIR}/${COMPRESSED_BACKUP_NAME}
echo -e "[info] BACKUP_FILE_PATH=${BACKUP_FILE_PATH}"

# Retrieve the remote backup
export REMOTE_BACKUP_DIR=:b2:$BUCKET/${BUCKET_PREFIX}/${COMPRESSED_BACKUP_NAME}
echo -e "[info] retrieving SQLite backup from Backblaze under account [${B2_ACCOUNT_ID}]..."
echo -e "[info] retrieving backup [${REMOTE_BACKUP_DIR}]"
rclone copy \
  --b2-account $B2_ACCOUNT_ID \
  --b2-key $B2_KEY \
  ${REMOTE_BACKUP_DIR} \
  ${BACKUP_SCRATCH_DIR}

# Print backup size
export BACKUP_SIZE=$(du -hs ${BACKUP_FILE_PATH})
echo -e "[info] Backup size: [${BACKUP_SIZE}]"

# Unzip the backup
echo -e "[info] Unzipping backup..."
cd ${BACKUP_SCRATCH_DIR} && tar -xf ${COMPRESSED_BACKUP_NAME}

# Determine the backup root dir
export BACKUP_DIR=${BACKUP_SCRATCH_DIR}/${BACKUP_NAME}
echo -e "[info] BACKUP_DIR=${BACKUP_DIR}"

# Decompress the postgres backup
export POSTGRES_PG_COMPRESSED_BACKUP_PATH=${BACKUP_DIR}/postgres.dumpall.sql.gz
gunzip ${POSTGRES_PG_COMPRESSED_BACKUP_PATH}
echo "[info] decompressing postgres backup from [${POSTGRES_PG_COMPRESSED_BACKUP_PATH}]..."

# Perform restore against DB
export POSTGRES_PG_BACKUP_PATH=${BACKUP_DIR}/postgres.dumpall.sql
echo "[info] Executing postgres restore against DB [${POSTGRES_PG_BACKUP_PATH}]..."
psql -f ${POSTGRES_PG_BACKUP_PATH} postgres
echo -e "[info] Database restore completed by executing file [${POSTGRES_PG_BACKUP_PATH}]"

echo "[info] Restore completed from [${REMOTE_BACKUP_DIR}]"
