FROM postgres:13.4-alpine3.14

# Install requirements and some creature comforts
RUN apk --no-cache add rclone curl file tree postgresql=13.4-r0

# Install kubectl
RUN echo -e "[info] Installing kubectl" && \
    curl -LO https://dl.k8s.io/release/v1.21.4/bin/linux/amd64/kubectl && \
    chmod +x kubectl && \
    mv kubectl /usr/bin/kubectl

# Copy in the entrypoint
COPY ./13.4-alpine3.14/docker-entrypoint.sh /docker-entrypoint.sh
COPY ./13.4-alpine3.14/backup.ash /backup.ash
COPY ./13.4-alpine3.14/restore.ash /restore.ash

ENTRYPOINT [ "/docker-entrypoint.sh" ]

LABEL org.opencontainers.image.authors="vados@vadosware.io"
LABEL version="alpine-3.14.2"
LABEL description="backup-sidecar-postgres - a sidecar for backing up postgres"
