# alpine:latest (amd64) as of 11/11/2021
FROM alpine@sha256:69704ef328d05a9f806b6b8502915e6a0a4faa4d72018dc42343f511490daf8a AS download

ARG BASEROW_VERSION
ENV BASEROW_VERSION=${BASEROW_VERSION:-1.10.0}

RUN wget https://gitlab.com/bramw/baserow/-/archive/${BASEROW_VERSION}/baserow-${BASEROW_VERSION}.tar.gz && \
    tar -xf baserow-${BASEROW_VERSION}.tar.gz && \
    mv baserow-${BASEROW_VERSION} /baserow


# node:16.15.0-slim (amd64) as of 2022/05/14
FROM node@sha256:3a4243f6c0cac673c7829a9a875ed599063e001bac9a38e82f1c31561dc3ffae

COPY --from=download /baserow /baserow

## NOTE: we can't remove premium because Nuxt uses it
## (https://gitlab.com/bramw/baserow/-/blob/develop/premium/web-frontend/modules/baserow_premium/module.js)
##
##  FATAL  Cannot find module '@/../premium/web-frontend/modules/baserow_premium/module.js'
##  Require stack:
##  - /baserow/web-frontend/node_modules/@nuxt/core/dist/core.js
# RUN rm -rf /baserow/premium

ARG UID
ENV UID=${UID:-9999}
ARG GID
ENV GID=${GID:-9999}

# Perform all OS package installation and cleanup in one single command to reduce the
# size of the created layer.
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    build-essential \
    curl \
    gnupg2 \
    dos2unix \
    tini \
    python3 \
    && apt-get autoclean \
    && apt-get clean \
    && apt-get autoremove \
    && rm -rf /var/lib/apt/lists/*

# The node image already creates a non-root user to run as, update its ids so they
# match the provided UID and GID we wish to build and run this image with.
# If GID or UID already exist that's OK no need to stop the build.
RUN groupmod -g ${GID} node || exit 0
RUN usermod -u ${UID} -g ${GID} node || exit 0

# Allow user to do anything they want in /baserow
RUN chown $UID -R /baserow
RUN chgrp $GID -R /baserow

USER $UID:$GID

WORKDIR /baserow/web-frontend

# We still need dev-dependencies as we will be running a nuxt build below
RUN yarn install

RUN yarn run build-local

RUN dos2unix /baserow/web-frontend/docker/docker-entrypoint.sh && \
    chmod a+x /baserow/web-frontend/docker/docker-entrypoint.sh

# tini installed above protects us from zombie processes and ensures the default signal
# handlers work, see https://github.com/krallin/tini.
ENTRYPOINT ["/usr/bin/tini", "--", "/bin/bash", "/baserow/web-frontend/docker/docker-entrypoint.sh"]
CMD ["nuxt"]
