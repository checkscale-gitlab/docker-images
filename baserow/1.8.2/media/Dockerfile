# alpine:latest (amd64) as of 11/11/2021
FROM alpine@sha256:69704ef328d05a9f806b6b8502915e6a0a4faa4d72018dc42343f511490daf8a AS download

ARG BASEROW_VERSION
ENV BASEROW_VERSION=${BASEROW_VERSION:-1.8.2}

RUN wget https://gitlab.com/bramw/baserow/-/archive/${BASEROW_VERSION}/baserow-${BASEROW_VERSION}.tar.gz && \
    tar -xf baserow-${BASEROW_VERSION}.tar.gz && \
    mv baserow-${BASEROW_VERSION} /baserow

FROM nginx:1.19.10

ARG UID
ENV UID=${UID:-9999}
ARG GID
ENV GID=${GID:-9999}

# The nginx image already creates a non-root user to run as, update its ids so they
# match the provided UID and GID we wish to build and run this image with.
# If GID or UID already exist that's OK no need to stop the build.
RUN groupmod -g ${GID} nginx
RUN usermod -u ${UID} -g ${GID} nginx

RUN rm /etc/nginx/conf.d/default.conf
COPY --from=download /baserow/media/nginx.conf /etc/nginx/conf.d
